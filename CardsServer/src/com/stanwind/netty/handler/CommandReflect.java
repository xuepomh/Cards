package com.stanwind.netty.handler;

import com.stanwind.netty.api.Log;
import com.stanwind.netty.constant.Command;
import com.stanwind.netty.constant.ServerSetting;
import com.stanwind.netty.core.protocol.model.Request;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Component
public class CommandReflect implements ServerSetting {
    //存放cmd对应的相应类名
    private Map<Short, String> reflect;

    @Autowired
    private Log log;
    /**
     * 初始化映射表
     */
    /**
     static {
     init();
     }
     */

    /**
     * 初始化映射表
     */
    @PostConstruct
    public void init() {
        int c = 0;
        reflect = new HashMap<Short, String>();                     //  新建映射表
        Field[] fields = Command.class.getFields();                 //  获取所有常量
        for (Field f : fields) {                                    //  遍历常量类型
            try {
                String name = f.getName();                          //  拿到常量名
                Short value = f.getShort(f.getName());              //  根据常量名获取常量值
                log.getLogger().debug("CMD Init---->" + name);
                name = HANDLER + "." + (name.split("_"))[1];        //  取得映射处理对象名
                c++;
                reflect.put(value, name);                           //  加入映射表
                log.getLogger().debug("Package---->" + name);
            } catch (IllegalArgumentException e) {
                log.getLogger().error("IllegalArgumentException", e);
            } catch (IllegalAccessException e) {
                log.getLogger().error("IllegalAccessException", e);

            }
        }
        log.getLogger().info("初始化[" + c + "]条指令集完毕");
    }

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * 通过反射执行处理类
     */
    public void handle(Short cmd, ChannelHandlerContext ctx, Request request) {
        String className;
        synchronized (reflect) {
            className = reflect.get(cmd);   //  取得处理对象路径
        }
        if (null == className || className.equals("")) {
            log.getLogger().error("未知的CMD[" + String.format("0x%H", cmd) + "]");
        }

        try {
            @SuppressWarnings("unchecked")
            Class<Handler> classtype = (Class<Handler>) Class.forName(className);   //  拿到类
            //Handler h = classtype.newInstance();                                  //  实例化
            Handler h = applicationContext.getBean(classtype);

            //  置入数据
            h.setCtx(ctx);
            h.setRequest(request);
            h.handle();                                                             //  调用处理
        } catch (ClassNotFoundException e) {
            log.getLogger().warn("找不到处理类[" + className + "] CMD[" + String.format("0x%H", cmd) + "] ", e);
        } catch (Exception e) {
            log.getLogger().error("处理对象出错[" + className + "] CMD[" + String.format("0x%H", cmd) + "]", e);
        }

    }


}
