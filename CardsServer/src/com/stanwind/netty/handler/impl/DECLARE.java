package com.stanwind.netty.handler.impl;

import com.stanwind.netty.api.ClientUtils;
import com.stanwind.netty.api.Log;
import com.stanwind.netty.api.RoomUtils;
import com.stanwind.netty.constant.Command;
import com.stanwind.netty.constant.GameStep;
import com.stanwind.netty.constant.ServerSetting;
import com.stanwind.netty.core.protocol.model.Response;
import com.stanwind.netty.domain.pojo.Player;
import com.stanwind.netty.domain.pojo.Room;
import com.stanwind.netty.handler.Handler;
import com.stanwind.netty.serialize.Declare;
import com.stanwind.netty.task.DeclareSecTask;
import com.stanwind.netty.task.DeclareTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Timer;

/**
 * Created by StanWind on 2017/2/20.
 * 叫地主处理
 */
@Service
@Scope("prototype")
public class DECLARE extends Handler {

    @Autowired
    private ClientUtils clientUtils;

    @Autowired
    private RoomUtils roomUtils;

    @Autowired
    private Log log;

    @Autowired
    ApplicationContext context;


    @Override
    public void handle() {
        Player player = clientUtils.getPlayer(getCtx().getChannel().getId());
        if (player == null)
            return;
        //获取玩家房间步骤
        Room room = roomUtils.getRoomByRoomId(player.getRoomId());
        GameStep gameStep = room.getGameStep();
        if (!room.getCurrentPlayer().equals(player)) {
            log.getLogger().info("不是当前玩家操作 -> " + player.getCharacter().getNickname());
            return;
        }

        if (room.getTimer() != null)
            room.getTimer().cancel();   //取消上次任务
        room.setTimer(new Timer());

        Declare declare = new Declare();
        declare.readFromBytes(getRequest().getData());

        if (gameStep.getValue() == GameStep.DECLARE_LANDLORD.getValue()) {
            //第一轮叫地主
            //跳到下个玩家
            Player nextPlayer = room.getNextPlayer();
            if (declare.getState() == 0) {
                //不叫地主
                log.getLogger().info("第一轮 不叫地主->" + player.getCharacter().getNickname());
                //判断是否结束叫地主
                if (nextPlayer.equals(room.getLandlord())) {
                    //一轮结束
                    if (room.getLandlordList().size() == 0) {
                        roomUtils.sendLandlord(room, "");
                        //TODO 没人叫地主 重新发牌
                        roomUtils.startGame(room.getRoomID());
                        log.getLogger().info("第一轮 没人叫地主 重新发牌");
                    } else if (room.getLandlordList().size() == 1) {
                        //第一轮一名玩家叫地主 地主归他
                        room.setLandlord(room.getLandlordList().get(0));
                        roomUtils.sendLandlord(room, room.getLandlord().getCharacter().getAccountId());
                        //TODO 开始出牌
                        roomUtils.startPayCards(room);
                        log.getLogger().info("第一轮 一人叫地主 地主是->" + room.getLandlordList().get(0).getCharacter().getNickname());
                    } else {
                        //TODO 开始第二轮叫地主
                        roomUtils.startDeclareSec(room);
                        nextPlayer = room.getLandlordList().get(0);
                        room.setCurrentPlayer(nextPlayer);
                        roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId()
                                , ServerSetting.DECLARE_TIME);
                        log.getLogger().info("开始第二轮叫地主 首位玩家->" + nextPlayer.getCharacter().getNickname());
                    }
                } else {
                    //跳到下个玩家叫地主
                    room.setCurrentPlayer(nextPlayer);
                    roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId(), ServerSetting.DECLARE_TIME);
                    log.getLogger().info("第一轮 跳到下位玩家->" + nextPlayer.getCharacter().getNickname());
                    room.getTimer().schedule(context.getBean(DeclareTask.class,room), ServerSetting.DECLARE_TIME);
                }
            } else {
                //叫地主
                room.getLandlordList().add(player);
                room.setCurrentPlayer(nextPlayer);
                room.setRate(room.getRate() * 2);
                log.getLogger().info("第一轮 叫地主->" + player.getCharacter().getNickname());

                //第一轮结束
                if (nextPlayer.equals(room.getLandlord())) {
                    if (room.getLandlordList().size() == 1) {
                        //第一轮一名玩家叫地主 地主归他
                        room.setLandlord(room.getLandlordList().get(0));
                        roomUtils.sendLandlord(room, room.getLandlord().getCharacter().getAccountId());
                        //TODO 开始出牌
                        roomUtils.startPayCards(room);
                        log.getLogger().info("第一轮 一人叫地主 地主是->" + room.getLandlordList().get(0).getCharacter().getNickname());
                    } else {
                        //TODO 开始第二轮叫地主
                        roomUtils.startDeclareSec(room);
                        nextPlayer = room.getLandlordList().get(0);
                        room.setCurrentPlayer(nextPlayer);
                        log.getLogger().info("开始第二轮叫地主 首位玩家->" + nextPlayer.getCharacter().getNickname());
                        roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId()
                                , ServerSetting.DECLARE_TIME);
                    }
                } else {
                    log.getLogger().info("第一轮 跳到下位玩家->" + nextPlayer.getCharacter().getNickname());
                    //跳到下个玩家叫地主
                    room.setCurrentPlayer(nextPlayer);
                    roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId(), ServerSetting.DECLARE_TIME);
                    room.getTimer().schedule(context.getBean(DeclareTask.class,room), ServerSetting.DECLARE_TIME);
                }
            }

            //群发叫地主状态
            declare.setAccountId(player.getCharacter().getAccountId());
            declare.setRate(room.getRate());
            Response response = new Response();
            response.setCmd(Command.CMD_DECLARE);
            response.setData(declare.getBytes());
            roomUtils.boardResponse(room, response);
            return;
        } else if (gameStep.getValue() == GameStep.DECLARE_LANDLORD_SEC.getValue()) {
            //群发叫地主状态
            declare.setAccountId(player.getCharacter().getAccountId());
            declare.setRate(room.getRate());
            Response response = new Response();
            response.setCmd(Command.CMD_DECLARE);
            response.setData(declare.getBytes());
            roomUtils.boardResponse(room, response);

            if (declare.getState() == 0) {
                if (room.getLandlordList().size() == 3) {
                    if (room.getLandlord().equals(player)) {
                        //三个人都叫 第一个玩家不叫
                        Player nextPlayer = room.getNextPlayer(room.getLandlordList(), player);
                        room.setCurrentPlayer(nextPlayer);
                        log.getLogger().info("第二轮叫地主 下一位玩家->" + nextPlayer.getCharacter().getNickname());
                        roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId()
                                , ServerSetting.DECLARE_TIME);
                        room.getTimer().schedule(context.getBean(DeclareSecTask.class,room), ServerSetting.DECLARE_TIME);
                        return;
                    } else {
                        //第二个玩家不叫 地主为第三个玩家
                        Player landlordPlayer = room.getNextPlayer(room.getLandlordList(), player);
                        room.setLandlord(landlordPlayer);
                        roomUtils.sendLandlord(room, landlordPlayer.getCharacter().getAccountId());
                        log.getLogger().info("第二轮 当前2玩家不叫 选定地主为下位3玩家->" + landlordPlayer.getCharacter().getNickname());
                        //TODO 开始出牌
                        roomUtils.startPayCards(room);
                        return;
                    }
                } else {
                    //只存在第一个玩家超时下个玩家 所以地主为第二个玩家
                    Player landlordPlayer = room.getLandlordList().get(1);
                    room.setLandlord(landlordPlayer);
                    roomUtils.sendLandlord(room, landlordPlayer.getCharacter().getAccountId());
                    log.getLogger().info("第二轮 当前玩家不叫 选定地主为下位玩家->" + landlordPlayer.getCharacter().getNickname());
                    //TODO 开始出牌
                    roomUtils.startPayCards(room);
                    return;
                }
            } else {
                Player landlordPlayer = player;
                room.setLandlord(landlordPlayer);
                room.setRate(room.getRate() * 2);
                roomUtils.sendLandlord(room, landlordPlayer.getCharacter().getAccountId());
                log.getLogger().info("第二轮 选定地主为当前玩家->" + landlordPlayer.getCharacter().getNickname());
                //TODO 开始出牌
                roomUtils.startPayCards(room);
                return;
            }
        } else {
            log.getLogger().info("玩家->【" + player.getCharacter().getNickname() + "】超时操作");
        }
    }
}
