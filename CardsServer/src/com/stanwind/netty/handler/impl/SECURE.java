package com.stanwind.netty.handler.impl;

import com.stanwind.netty.handler.Handler;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by StanWind on 2017/2/17.
 */
@Service
@Scope("prototype")
public class SECURE extends Handler {
    @Override
    public void handle() {

    }
}
