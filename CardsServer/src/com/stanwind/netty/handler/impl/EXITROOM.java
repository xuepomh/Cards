package com.stanwind.netty.handler.impl;

import com.stanwind.netty.api.ClientUtils;
import com.stanwind.netty.api.Log;
import com.stanwind.netty.api.RoomUtils;
import com.stanwind.netty.domain.pojo.Player;
import com.stanwind.netty.domain.pojo.Room;
import com.stanwind.netty.handler.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by StanWind on 2017/2/18.
 */
@Service
@Scope("prototype")
public class EXITROOM extends Handler {
    @Autowired
    private ClientUtils clientUtils;

    @Autowired
    private RoomUtils roomUtils;

    @Autowired
    private Log log;

    @Override
    public void handle() {
        Player player = clientUtils.getPlayer(getCtx().getChannel().getId());
        if (player == null)
            return;

        String name = player.getCharacter().getNickname();
        if (player.getRoomId() > 0) {
            //从房间退出
            if (roomUtils.exitRoom(player)) {
                log.getLogger().info("正常退出");
                //删除缓存数据
                clientUtils.del(player);
            }else{
                //FIXME 退出重置房间
                Room room = roomUtils.getRoomByRoomId(player.getRoomId());
                room.resetRoom();

                log.getLogger().info("游戏中途退出->"+player.getCharacter().getNickname());
            }
        } else {
            log.getLogger().info("不在房间->" + name);
        }
    }
}
