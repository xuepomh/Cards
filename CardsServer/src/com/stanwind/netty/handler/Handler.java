package com.stanwind.netty.handler;

import com.stanwind.netty.constant.Command;
import com.stanwind.netty.core.protocol.model.Request;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * 分发处理基类
 *
 * @author StanWind
 */
public abstract class Handler implements Command {
    protected ChannelHandlerContext ctx;
    protected Request request;

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    /**
     * 分发处理方法
     */
    public abstract void handle();

}
