package com.stanwind.netty;

import com.stanwind.netty.constant.ServerSetting;
import com.stanwind.netty.task.RunningTask;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Timer;

/**
 * Created by StanWind on 2017/2/16.
 */
//@SpringBootApplication
////@Configuration//配置控制
////@EnableAutoConfiguration//启用自动配置
////@ComponentScan//组件扫描
//@ImportResource("classpath:spring-config.xml")
public class Application {
    public static int currentTime;

    public static void main(String[] args){
        ApplicationContext applicationContext =  new ClassPathXmlApplicationContext("spring-config.xml");
        //context.getBean("bootstrapServer");
        //开始本地计时
        currentTime = ServerSetting.INIT_TIME;
        Timer timer = new Timer();
        timer.schedule(new RunningTask(),0,1000);
//        //启动netty
//        ApplicationContext context = SpringApplication.run(Application.class, args);
//        context.getBean(BootstrapServer.class).serverStart();


        return;

    }
}
