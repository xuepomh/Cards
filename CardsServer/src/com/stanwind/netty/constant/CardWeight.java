package com.stanwind.netty.constant;

/**
 * Created by StanWind on 2017/2/14.
 */
public enum CardWeight implements BaseEnum{
    Three(1),           //  3
    Four(2),            //  4
    Five(3),            //  5
    Six(4),             //  6
    Seven(5),           //  7
    Eight(6),           //  8
    Nine(7),            //  9
    Ten(8),             //  10
    J(9),               //  J
    Q(10),              //  Q
    K(11),              //  K
    A(12),              //  A
    Two(13),            //  2
    Queen(14),          //  小王
    King(15);           //  大王

    private int value;

    CardWeight(int value) {
        this.value = value;
    }

    public CardWeight setValue(int value) {
        this.value = value; return this;
    }

    public int getValue() {
        return value;
    }

}
