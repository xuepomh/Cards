package com.stanwind.netty.constant;

/**
 * Created by StanWind on 2017/2/16.
 */
public interface ServerSetting {
    /**
     * 服务器名前缀
     */
    String SERVER_NAME_PRE = "cards:server:";

    /**
     * 当前服务器编号
     */
    String SERVER_NAME_ID = "A";

    /**
     * 当前服务器配置
     */
    String SERVER_IP="192.168.23.2";
    int SERVER_PORT=8001;
    /**
     * 用于生成token
     */
    String TOKENKEY = "stanwind";

    /**
     * 过期时间 单位 秒
     */
    int EXPIRE_TIME = 60000;

    /**
     * 协议前缀
     */
    int FLAG = 0x4D5A0000;

    //Handler Package
    String HANDLER = "com.stanwind.netty.handler.impl";

    /**
     * 房间最多人
     */
    int ROOM_SIZE=  3;

    /**
     * 服务器初始化时间
     */
    int INIT_TIME = 1000000;

    /**
     * 叫地主时间
     */
    int DECLARE_TIME = 10*1000;

    /**
     * 出牌时间
     */
    int EAT_CARD_TIME = 30*1000;


}
