package com.stanwind.netty.constant;

/**
 * command short
 *
 * @author westwind
 */
public interface Command {
    //Request

    //heart beat
    short CMD_SECURE = 0x1FAB;
    //开始匹配游戏
    short CMD_GETPLAY = 0x3132;
    //退出房间
    short CMD_EXITROOM = 0x3232;

    //准备游戏
    short CMD_READY = 0x313A;

    //步骤告知
    short CMD_STEPCALL = 0x3236;
    //叫地主
    short CMD_DECLARE = 0x3237;
    //选定地主
    short CMD_PUB_LANDLORD = 0x321A;

    //出牌
    short CMD_PAYCARD = 0x313C;
    //地主牌
    short CMD_LANDLORD_CARD = 0x3FFF;


    //response
    //msgbox
    short CMD_MSGBOX = 0x2F31;
    //Someone com in
    short CMD_ADDPLAYER = 0x3FF9;

    //发牌包
    short CMD_DISPATCH = 0x313B;



    //显示所有玩家牌
    short CMD_SHOWCARDS = 0x6666;
    //金币更改
    short CMD_UPDATEMONEY = 0X3241;

//    //Time Sync 心跳
//    short CMD_TIMESYNC = 0x3532;
//    //Time Count Down
//    short CMD_TIMEDOWN = 0x4F21;
//    //Scroll Announce
//    short CMD_SCROLL = 0x2D1C;
//    //SETICON
//    short CMD_SETICON = 0x2C2F;
//    //GOLDRANK
//    short CMD_GOLDRANK = 0x2A2A;
//    //KICK OUT ROOM 当金币不足 不能进入房间 或者从房间踢出
//    short CMD_KICKOUTROOM = 0x1D5F;
//    //Exchange Diamond
//    short CMD_EXCHANGEDIAMOND = 0x1D4A;
//    //广告激励
//    short CMD_ADAWARD = 0x1D3B;
//    //Lucky
//    short CMD_LUCKYAWARD = 0x1D2C;
}
