package com.stanwind.netty.constant;

/**
 * Created by StanWind on 2017/2/18.
 */
public enum GameStep {
    DECLARE_LANDLORD(1),    //叫地主
    EAT_CARD(2),            //吃牌
    PAY_CARD(3),            //出牌
    DECLARE_LANDLORD_SEC(4);//第二轮叫地主

    private int value;

    GameStep(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
