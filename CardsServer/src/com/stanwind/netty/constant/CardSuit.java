package com.stanwind.netty.constant;

/**
 * Created by StanWind on 2017/2/14.
 */
public enum CardSuit implements BaseEnum{
    Blank(0),
    Clubs(1),      //  方块
    Diamonds(2),   //  梅花
    Hearts(3),     //  红桃
    Spades(4);     //  黑桃

    private int value;


    CardSuit(int value) {
        this.value = value;
    }

    public CardSuit setValue(int value) {
        this.value = value;
        return this;
    }

    public int getValue() {
        return value;
    }

}
