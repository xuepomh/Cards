package com.stanwind.netty.constant;

/**
 * Created by StanWind on 2017/2/18.
 */
public interface BaseEnum  {
    int getValue();
}
