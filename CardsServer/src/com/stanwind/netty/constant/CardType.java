package com.stanwind.netty.constant;

/**
 * Created by StanWind on 2017/2/22.
 */
public enum CardType {
    SINGLE_CARD, //单牌
    DOUBLE_CARD, //对子
    THREE_CARD, //3不带
    BOMB_CARD,      //炸弹（四张）
    KING_BOMB_CARD,     //王炸
    THREE_ONE_CARD,     //三带一
    THREE_TWO_CARD,     //三带二
    FOUR_TWO_CARD,      //四带俩单
    FOUR_TWOTWO_CARD,  //四带俩对
    LIAN_CARD,          //顺子
    LIAN_DUI_CARD,      //连队
    FEIJI_NO_CARD,      //飞机不带
    FEIJI_SINGLE_CARD,  //飞机带单
    FEIJI_DOUBLE_CARD,  //飞机带对
    ERROR_CARD          //错误类型
}
