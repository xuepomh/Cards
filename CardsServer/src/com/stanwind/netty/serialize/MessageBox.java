package com.stanwind.netty.serialize;

import com.stanwind.netty.core.protocol.core.Serializer;

/**
 * Created by StanWind on 2017/2/17.
 */
public class MessageBox extends Serializer {

    //0调回登录界面 1跳回主界面 2正常提示框
    private int state;

    private String message;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        writeByte((byte)state);
        writeString(message);
    }
}
