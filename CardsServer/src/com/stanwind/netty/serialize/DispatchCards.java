package com.stanwind.netty.serialize;

import com.stanwind.netty.core.protocol.core.Serializer;
import com.stanwind.netty.domain.pojo.Card;

import java.util.List;

/**
 * Created by StanWind on 2017/2/18.
 * 发牌包
 */
public class DispatchCards extends Serializer {

    private List<Card> cards;

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        //牌的张数
        writeByte((byte) cards.size());
        for (Card card : cards) {
            writeByte((byte) card.getWeight().getValue());
            writeByte((byte) card.getSuit().getValue());
        }
    }
}
