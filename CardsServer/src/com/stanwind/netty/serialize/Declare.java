package com.stanwind.netty.serialize;

import com.stanwind.netty.core.protocol.core.Serializer;

/**
 * Created by StanWind on 2017/2/20.
 */
public class Declare extends Serializer {
    private String accountId;
    private int state;      // 0不叫 1叫地主
    private int rate;       //  倍数

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    protected void read() {
        state = (int)readByte();

    }

    @Override
    protected void write() {
        writeByte((byte)state);
        writeString(accountId);
        writeInt(rate);
    }
}
