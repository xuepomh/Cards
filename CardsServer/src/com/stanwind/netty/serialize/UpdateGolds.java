package com.stanwind.netty.serialize;

import com.stanwind.netty.core.protocol.core.Serializer;
import com.stanwind.netty.domain.pojo.Player;

import java.util.List;

/**
 * Created by StanWind on 2017/2/25.
 */
public class UpdateGolds extends Serializer {
    private List<Player> players;

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        for(Player player : players){
            writeString(player.getCharacter().getAccountId());
            writeInt(player.getChangedGolds());
        }

    }
}
