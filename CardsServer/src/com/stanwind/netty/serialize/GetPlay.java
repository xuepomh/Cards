package com.stanwind.netty.serialize;

import com.stanwind.netty.api.Log;
import com.stanwind.netty.core.protocol.core.Serializer;
import com.stanwind.netty.domain.pojo.Player;
import com.stanwind.netty.domain.pojo.Room;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by StanWind on 2017/2/17.
 */
public class GetPlay extends Serializer {

    //Request

    private String token;


    //Response
    private Room room;

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    protected void read() {
        setToken(readString());
    }


    @Override
    protected void write() {
        //房间号
        writeInt(room.getRoomID());
        //是否正在游戏
        if (room.isGame())
            writeByte((byte) 1);
        else
            writeByte((byte) 0);

        List<Player> playerList = room.getPlayers();
        int i = 0;
        for (Player player : playerList) {
            i++;
            if (player != null) {
                System.out.printf(player.toString());
                writeByte((byte) i);                    //  座位号
                writeString(player.getCharacter().getAccountId());    //  玩家唯一ID
                writeString(player.getCharacter().getIconSource());   //  头像
                writeString(player.getCharacter().getNickname());      //  NickName
                writeInt(player.getCharacter().getGold());             //  Gold
                if (player.isReady()) {
                    writeByte((byte) 1);                //  准备
                } else {
                    writeByte((byte) 0);                //  未准备
                }

            }
        }


    }
}
