package com.stanwind.netty.serialize;

import com.stanwind.netty.constant.GameStep;
import com.stanwind.netty.core.protocol.core.Serializer;

/**
 * Created by StanWind on 2017/2/18.
 * 告知玩家操作
 */
public class StepCall extends Serializer{
    private GameStep gameStep;  //操作步骤
    private int operatingTime;  //操作时间
    private String accountId;   //操作人

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public int getOperatingTime() {
        return operatingTime;
    }

    public void setOperatingTime(int operatingTime) {
        this.operatingTime = operatingTime;
    }

    public GameStep getGameStep() {
        return gameStep;
    }

    public void setGameStep(GameStep gameStep) {
        this.gameStep = gameStep;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        writeByte((byte)gameStep.getValue());   //操作类型
        writeByte((byte)(operatingTime/1000));         //操作时间
        writeString(accountId);                 //操作人ID
    }
}
