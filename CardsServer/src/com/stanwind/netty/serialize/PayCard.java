package com.stanwind.netty.serialize;

import com.stanwind.netty.api.NumberUtils;
import com.stanwind.netty.constant.CardSuit;
import com.stanwind.netty.constant.CardWeight;
import com.stanwind.netty.core.protocol.core.Serializer;
import com.stanwind.netty.domain.pojo.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by StanWind on 2017/2/21.
 * 出牌收发
 */
public class PayCard extends Serializer {
    private String accountId;
    private List<Card> payCards;
    private int rate;

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public List<Card> getPayCards() {
        return payCards;
    }

    public void setPayCards(List<Card> payCards) {
        this.payCards = payCards;
    }


    @Override
    protected void read() {
        NumberUtils numberUtils = new NumberUtils();
        payCards = new ArrayList<>();
        int count = readShort();
        for (int i = 0; i < count; i++) {
            Card card = new Card();
            card.setWeight((CardWeight) numberUtils.enumParse(CardWeight.class, (int) readByte()));
            card.setSuit((CardSuit) numberUtils.enumParse(CardSuit.class, (int) readByte()));
            payCards.add(card);
        }
    }

    @Override
    protected void write() {
        writeString(accountId);
        writeInt(rate);
        writeShort((short) payCards.size());
        for (Card card : payCards) {
            writeByte((byte) card.getWeight().getValue());
            writeByte((byte) card.getSuit().getValue());
        }
    }
}
