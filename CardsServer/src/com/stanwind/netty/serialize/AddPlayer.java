package com.stanwind.netty.serialize;

import com.stanwind.netty.api.RoomUtils;
import com.stanwind.netty.core.protocol.core.Serializer;
import com.stanwind.netty.domain.pojo.Player;
import com.stanwind.netty.domain.pojo.Room;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by StanWind on 2017/2/17.
 */
public class AddPlayer extends Serializer {
    private Player player;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        writeByte((byte)player.getSeatId());                    //  座位号
        writeString(player.getCharacter().getAccountId());      //  玩家唯一ID
        writeString(player.getCharacter().getIconSource());     //  头像
        writeString(player.getCharacter().getNickname());       //  NickName
        writeInt(player.getCharacter().getGold());              //  Gold
        if (player.isReady()) {
            writeByte((byte) 1);                //  准备
        } else {
            writeByte((byte) 0);                //  未准备
        }
    }
}
