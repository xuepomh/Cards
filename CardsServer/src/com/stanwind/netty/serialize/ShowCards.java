package com.stanwind.netty.serialize;

import com.stanwind.netty.core.protocol.core.Serializer;
import com.stanwind.netty.domain.pojo.Card;
import com.stanwind.netty.domain.pojo.Player;

import java.util.List;

/**
 * Created by StanWind on 2017/2/25.
 */
public class ShowCards extends Serializer {
    private List<Player> players;

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        for(Player player: players){
            List<Card> temp = player.getHandCards();
            //手牌为0不发
            if(temp.size()!=0){
                writeString(player.getCharacter().getAccountId());
                writeShort((short) temp.size());
                for (Card card : temp) {
                    writeByte((byte) card.getWeight().getValue());
                    writeByte((byte) card.getSuit().getValue());
                }
            }
        }

    }
}
