package com.stanwind.netty.serialize;

import com.stanwind.netty.core.protocol.core.Serializer;

/**
 * Created by StanWind on 2017/2/18.
 */
public class DelPlayer extends Serializer {
    private String accountId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        writeString(accountId);
    }
}
