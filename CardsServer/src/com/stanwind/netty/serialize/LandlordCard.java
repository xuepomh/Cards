package com.stanwind.netty.serialize;

import com.stanwind.netty.core.protocol.core.Serializer;
import com.stanwind.netty.domain.pojo.Card;
import com.stanwind.netty.domain.pojo.Room;

/**
 * Created by StanWind on 2017/2/21.
 * 地主三张牌显示
 */
public class LandlordCard extends Serializer {
    private Room room;
    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    protected void read() {

    }

    @Override
    protected void write() {
        writeString(room.getLandlord().getCharacter().getAccountId());
        for (Card card : room.getCards()) {
            writeByte((byte) card.getWeight().getValue());
            writeByte((byte) card.getSuit().getValue());
        }

    }

}
