package com.stanwind.netty.service;

import com.stanwind.netty.domain.model.Character;

/**
 * Created by StanWind on 2017/2/15.
 */
public interface CharacterService {
    int deleteByAccountIdKey(String accountId);

    int insert(Character record);

    Character selectByAccountId(String accountId);

    int updateByAccountId(Character record);
}
