package com.stanwind.netty.service.impl;

import com.stanwind.netty.domain.dao.AccountMapper;
import com.stanwind.netty.domain.model.Account;
import com.stanwind.netty.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by StanWind on 2017/2/14.
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public void update(Account account) {
        this.accountMapper.updateByPrimaryKey(account);
    }

    @Override
    public Account getAccountByAccount(String account) {
        return this.accountMapper.selectByAccount(account);
    }

    @Override
    public List<Account> listAll() {
        return this.accountMapper.listAll();
    }

    @Override
    public void insert(Account account) {
        this.accountMapper.insert(account);
    }

    @Override
    public Account getAccountByAccountID(String accountId) {
        return this.accountMapper.selectByPrimaryKey(accountId);
    }
}
