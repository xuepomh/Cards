package com.stanwind.netty.service.impl;

import com.stanwind.netty.domain.dao.CharacterMapper;
import com.stanwind.netty.domain.model.Character;
import com.stanwind.netty.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by StanWind on 2017/2/15.
 */
@Service("CharacterService")
public class CharacterServiceImpl implements CharacterService {
    @Autowired
    private CharacterMapper characterMapper;

    @Override
    public int deleteByAccountIdKey(String accountId) {
        return this.characterMapper.deleteByPrimaryKey(accountId);
    }

    @Override
    public int insert(Character record) {
        return this.characterMapper.insert(record);
    }

    @Override
    public Character selectByAccountId(String accountId) {
        return this.characterMapper.selectByPrimaryKey(accountId);
    }

    @Override
    public int updateByAccountId(Character record) {
        return this.characterMapper.updateByPrimaryKey(record);
    }
}
