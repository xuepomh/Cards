package com.stanwind.netty.service;

import com.stanwind.netty.domain.model.Account;

import java.util.List;

/**
 * Created by StanWind on 2017/2/14.
 */
public interface AccountService {
    /**
     * 根据用户账号查询账户信息
     * @param account 账号
     * @return Account 对象
     */
    Account getAccountByAccount(String account);

    /**
     * 根据accountID获取账户对象
     * @param accountId accountID
     * @return  Account对象
     */
    Account getAccountByAccountID(String accountId);

    /**
     * 列出所有账户
     * @return List账户
     */
    List<Account> listAll();

    /**
     * 插入新用户
     * @param account Account对象
     */
    void insert(Account account);

    /**
     * 更新用户
     * @param account Account对象
     */
    void update(Account account);

}
