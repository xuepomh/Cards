package com.stanwind.netty.domain.pojo;

import java.util.List;

/**
 * Created by StanWind on 2017/2/22.
 */
public class CardTypeIndex {
    public List<Integer> single_index;
    public List<Integer> double_index;
    public List<Integer> three_index;
    public List<Integer> four_index;
}
