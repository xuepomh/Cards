package com.stanwind.netty.domain.pojo;

import com.stanwind.netty.constant.CardSuit;
import com.stanwind.netty.constant.CardWeight;

/**
 * Created by StanWind on 2017/2/18.
 */
public class Card implements Comparable<Card> {
    private CardWeight weight;
    private CardSuit suit;

    public CardWeight getWeight() {
        return weight;
    }

    public void setWeight(CardWeight weight) {
        this.weight = weight;
    }

    public CardSuit getSuit() {
        return suit;
    }

    public void setSuit(CardSuit suit) {
        this.suit = suit;
    }

    @Override
    public String toString() {
        return "{" + weight + "." + suit + "}";
    }

    @Override
    public int compareTo(Card o) {
        int compare = 0;

        int result = this.getWeight().getValue() - o.getWeight().getValue();
        if (result > 0) {
            compare = 1;
        } else if (result < 0) {
            compare = -1;
        } else if (result == 0) {
            //  不比花色
            result = this.getSuit().getValue() - o.getSuit().getValue();
            if (result > 0) {
                compare = 1;
            } else if (result < 0) {
                compare = -1;
            }
            compare = 0;
        }
        return compare*-1;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Card)
            return (((Card) obj).getSuit().getValue() == getSuit().getValue()
                    && ((Card) obj).getWeight().getValue() ==getWeight().getValue());
        return false;
    }
}
