package com.stanwind.netty.domain.pojo;


import com.stanwind.netty.Application;
import com.stanwind.netty.constant.GameStep;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;

/**
 * Created by StanWind on 2017/2/17.
 */
public class Room {
    private int roomID;
    private List<Player> players;
    private boolean isGame;
    private List<Card> cards;
    private Random random = new Random();
    private Timer timer;


    //数据类
    private int rate = 1;
    private int stepTime = -1;
    private Player currentPlayer;
    private Player landlord;
    private GameStep gameStep;
    private List<Player> landlordList;  //备选庄家
    private Player lastPlayer;          //上个出牌的玩家
    private List<Card> lastCards;       //上个玩家的出牌

    public Player getLastPlayer() {
        return lastPlayer;
    }

    public void setLastPlayer(Player lastPlayer) {
        this.lastPlayer = lastPlayer;
    }


    public List<Card> getLastCards() {
        return lastCards;
    }

    public void setLastCards(List<Card> lastCards) {
        this.lastCards = lastCards;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public GameStep getGameStep() {
        return gameStep;
    }

    public void setGameStep(GameStep gameStep) {
        this.gameStep = gameStep;
    }

    public List<Player> getLandlordList() {
        return landlordList;
    }

    public void setLandlordList(List<Player> landlordList) {
        this.landlordList = landlordList;
    }

    public Player getLandlord() {
        return landlord;
    }

    public void setLandlord(Player landlord) {
        this.landlord = landlord;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getStepTime() {
        return stepTime;
    }

    public void setStepTime(int stepTime) {
        this.stepTime = stepTime;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public boolean isGame() {
        return isGame;
    }

    public void setGame(boolean game) {
        isGame = game;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public Room() {
        players = new ArrayList<Player>();
    }


    /**
     * 获取随机牌
     *
     * @return
     */
    public Card getRandCard() {
        int cardsCount = cards.size();
        int index = random.nextInt(cardsCount);
        Card c = cards.get(index);
        cards.remove(index);
        return c;
    }


    /**
     * 初始化房间数据并且开始叫庄
     *
     * @param player 庄家对象
     */
    public void initGame(Player player) {
        //设定倍率
        rate = 15;
        //地主
        landlord = player;
        //地主备选名单
        landlordList = new ArrayList<>();
        //当前执行玩家
        currentPlayer = player;
        //步骤开始时间
        stepTime = Application.currentTime;
        //步骤
        gameStep = GameStep.DECLARE_LANDLORD;
    }

    /**
     * 获取下一位操作玩家
     * @return 玩家对象
     */
    public Player getNextPlayer() {
        return getNextPlayer(players, currentPlayer);
    }

    public Player getNextPlayer(List<Player> playerList, Player player) {
        int index = 0;
        for (Player p : playerList) {
            index++;
            if (p.equals(player))
                break;
        }

        if (index > playerList.size()) {
            return null;
        } else if (index == playerList.size()) {
            index = 0;
        }
        return playerList.get(index);
    }


    public void resetRoom(){
        setGame(false);
        for(Player player:players){
            player.setReady(false);
        }
    }
}
