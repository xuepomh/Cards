package com.stanwind.netty.domain.pojo;


import com.stanwind.netty.domain.model.Account;
import com.stanwind.netty.domain.model.Character;
import org.jboss.netty.channel.Channel;

import java.util.List;


/**
 * Created by StanWind on 2017/2/17.
 */
public class Player{
    private Channel channel;

    private List<Card> handCards;

    private boolean isReady;    //是否准备

    private boolean offline;

    private Character character;

    private int roomId;

    private int seatId;

    private int changedGolds;

    public int getChangedGolds() {
        return changedGolds;
    }

    public void setChangedGolds(int changedGolds) {
        this.changedGolds = changedGolds;
    }

    public Channel getChannel() {
        return channel;
    }

    public List<Card> getHandCards() {
        return handCards;
    }

    public void setHandCards(List<Card> handCards) {
        this.handCards = handCards;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public boolean isOffline() {
        return offline;
    }

    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    public int getSeatId() {
        return seatId;
    }

    public void setSeatId(int seatId) {
        this.seatId = seatId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    @Override
    public String toString() {
        return "Player{" +
                "isReady=" + isReady +
                ", character=" + character +
                ", roomId=" + roomId +
                ", seatId=" + seatId +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Player)
            return this.getCharacter().getAccountId().equals(((Player) obj).getCharacter().getAccountId());
        return false;
    }


}
