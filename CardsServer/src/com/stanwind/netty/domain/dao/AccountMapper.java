package com.stanwind.netty.domain.dao;

import com.stanwind.netty.domain.model.Account;

import java.util.List;

public interface AccountMapper {
    int deleteByPrimaryKey(String accountId);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(String accountId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);

    Account selectByAccount(String account);

    List<Account> listAll();
}