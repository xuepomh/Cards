package com.stanwind.netty.domain.dao;

import com.stanwind.netty.domain.model.Character;

public interface CharacterMapper {
    int deleteByPrimaryKey(String accountId);

    int insert(Character record);

    int insertSelective(Character record);

    Character selectByPrimaryKey(String accountId);

    int updateByPrimaryKeySelective(Character record);

    int updateByPrimaryKey(Character record);
}