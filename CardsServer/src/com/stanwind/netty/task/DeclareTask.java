package com.stanwind.netty.task;

import com.stanwind.netty.api.Log;
import com.stanwind.netty.api.RoomUtils;
import com.stanwind.netty.constant.Command;
import com.stanwind.netty.constant.GameStep;
import com.stanwind.netty.constant.ServerSetting;
import com.stanwind.netty.core.protocol.model.Response;
import com.stanwind.netty.domain.pojo.Player;
import com.stanwind.netty.domain.pojo.Room;
import com.stanwind.netty.serialize.Declare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by StanWind on 2017/2/20.
 * 叫地主检测器
 */
@Service
@Scope("prototype")
public class DeclareTask extends TimerTask {
    private Room room;

    public DeclareTask(Room room) {
        this.room = room;
    }


    @Autowired
    private RoomUtils roomUtils;
    @Autowired
    private Log log;
    @Autowired
    private ApplicationContext context;

    @Override
    public void run() {

        //当前步骤判断
        if (room.getGameStep() != GameStep.DECLARE_LANDLORD)
            return;

        if (room.getTimer() != null)
            room.getTimer().cancel();   //取消上次任务
        room.setTimer(new Timer());

        //群发当前玩家不叫
        Declare declare = new Declare();
        declare.setAccountId(room.getCurrentPlayer().getCharacter().getAccountId());
        declare.setRate(room.getRate());
        declare.setState(0);
        Response response = new Response();
        response.setCmd(Command.CMD_DECLARE);
        response.setData(declare.getBytes());
        roomUtils.boardResponse(room, response);
        log.getLogger().debug("【超时】第一轮玩家超时 不叫地主->"+room.getCurrentPlayer().getCharacter().getNickname());

        //跳到下个玩家
        Player nextPlayer = room.getNextPlayer();
        //判断是否结束叫地主
        if (nextPlayer.equals(room.getLandlord())) {
            log.getLogger().debug("【超时】第一轮结束");
            //一轮结束 判断储君个数
            if (room.getLandlordList().size() == 0) {
                roomUtils.sendLandlord(room, "");
                //TODO 没人叫地主 重新发牌
                roomUtils.startGame(room.getRoomID());
                log.getLogger().debug("【超时】没人叫地主 重新发牌");
            } else if (room.getLandlordList().size() == 1) {
                //TODO 第一轮 一名玩家叫地主 选定地主
                room.setLandlord(room.getLandlordList().get(0));
                roomUtils.sendLandlord(room, room.getLandlord().getCharacter().getAccountId());
                //TODO 开始出牌
                roomUtils.startPayCards(room);
                log.getLogger().debug("【超时】一人叫地主 地主是->"+room.getLandlordList().get(0).getCharacter().getNickname());
            } else {
                //开始第二轮叫地主
                roomUtils.startDeclareSec(room);
                //设置下个玩家
                nextPlayer = room.getLandlordList().get(0);
                room.setCurrentPlayer(nextPlayer);
                //发送操作许可
                roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId()
                        , ServerSetting.DECLARE_TIME);
                log.getLogger().debug("【超时】开始第二轮叫地主 首位玩家->"+nextPlayer.getCharacter().getNickname());
            }
            return;
        } else {
            room.setCurrentPlayer(nextPlayer);
            roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId(), ServerSetting.DECLARE_TIME);
            log.getLogger().debug("【超时】第一轮 跳到下位玩家->"+nextPlayer.getCharacter().getNickname());
            room.getTimer().schedule(context.getBean(DeclareTask.class,room), ServerSetting.DECLARE_TIME);
        }
    }





}




