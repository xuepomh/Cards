package com.stanwind.netty.task;

import com.stanwind.netty.api.Log;
import com.stanwind.netty.api.RoomUtils;
import com.stanwind.netty.constant.Command;
import com.stanwind.netty.constant.GameStep;
import com.stanwind.netty.constant.ServerSetting;
import com.stanwind.netty.core.protocol.model.Response;
import com.stanwind.netty.domain.pojo.Player;
import com.stanwind.netty.domain.pojo.Room;
import com.stanwind.netty.serialize.Declare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by StanWind on 2017/2/20.
 */
@Service
@Scope("prototype")
public class DeclareSecTask extends TimerTask {
    private Room room;

    public DeclareSecTask(Room room) {
        this.room = room;
    }

    @Autowired
    private RoomUtils roomUtils;

    @Autowired
    private Log log;

    @Autowired
    private ApplicationContext context;

    @Override
    public void run() {
        //  当前步骤判断
        if (room.getGameStep() != GameStep.DECLARE_LANDLORD_SEC)
            return;

        //  取消上次任务
        if (room.getTimer() != null)
            room.getTimer().cancel();
        room.setTimer(new Timer());

        //  群发当前玩家不叫
        Declare declare = new Declare();
        declare.setAccountId(room.getCurrentPlayer().getCharacter().getAccountId());
        declare.setState(0);
        declare.setRate(room.getRate());
        Response response = new Response();
        response.setCmd(Command.CMD_DECLARE);
        response.setData(declare.getBytes());
        roomUtils.boardResponse(room, response);

        log.getLogger().debug("【超时】第二轮 玩家超时 不叫地主->" + room.getCurrentPlayer().getCharacter().getNickname());
        Player player = room.getCurrentPlayer();
        log.getLogger().debug("第一轮size->" + room.getLandlordList().size());
        if (room.getLandlordList().size() == 3) {
            if (room.getLandlord().equals(player)) {
                //三个人都叫 第一个玩家不叫
                Player nextPlayer = room.getNextPlayer(room.getLandlordList(), player);
                room.setCurrentPlayer(nextPlayer);
                log.getLogger().debug("【超时】第二轮叫地主 下一位玩家->" + nextPlayer.getCharacter().getNickname());
                roomUtils.sendStep(room, nextPlayer.getCharacter().getAccountId()
                        , ServerSetting.DECLARE_TIME);
                room.getTimer().schedule(context.getBean(DeclareSecTask.class,room), ServerSetting.DECLARE_TIME);
            } else {
                //第二个玩家不叫 地主为第三个玩家
                Player landlordPlayer = room.getNextPlayer(room.getLandlordList(), player);
                room.setLandlord(landlordPlayer);
                roomUtils.sendLandlord(room, landlordPlayer.getCharacter().getAccountId());
                log.getLogger().debug("【超时】第二轮 当前2玩家不叫 选定地主为下位3玩家->" + landlordPlayer.getCharacter().getNickname());
                //TODO 开始出牌
                roomUtils.startPayCards(room);
            }

        } else {
            //只存在第一个玩家超时下个玩家 所以地主为第二个玩家
            Player landlordPlayer = room.getLandlordList().get(1);
            room.setLandlord(landlordPlayer);
            roomUtils.sendLandlord(room, landlordPlayer.getCharacter().getAccountId());
            //TODO 开始出牌
            roomUtils.startPayCards(room);
            log.getLogger().debug("【超时】第二轮 当前玩家不叫 选定地主为下位玩家->" + landlordPlayer.getCharacter().getNickname());
            return;
        }
    }

}
