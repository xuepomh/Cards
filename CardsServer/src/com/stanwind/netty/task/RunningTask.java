package com.stanwind.netty.task;

import com.stanwind.netty.Application;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.TimerTask;

/**
 * Created by StanWind on 2017/2/17.
 * 系统时间递增
 */
@Service
public class RunningTask extends TimerTask {
    @Override
    public void run() {
        Application.currentTime++;
    }
}
