package com.stanwind.netty.task;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.TimerTask;

/**
 * Created by StanWind on 2017/2/20.
 * 吃牌任务
 */
@Service
@Scope("prototype")
public class EatCardTask extends TimerTask {
    @Override
    public void run() {

    }
}
