package com.stanwind.netty.core.protocol.codc;

import com.stanwind.netty.constant.ServerSetting;
import com.stanwind.netty.core.protocol.model.Request;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

/**
 * 请求解码器
 *
 * @author StanWind
 *         FrameDecoder协助解决粘包、分包问题
 */
public class RequestDecoder extends FrameDecoder {
    /**
     * 数据包基础长度
     */
    public static int BASE_LENGTH = 4 + 4 + 2 + 2;
    //[4D 5A 00 00] [00 00 00 00] [LENGTH] [COMMAND] [DATA]

    @Override
    protected Object decode(ChannelHandlerContext context, Channel channel,
                            ChannelBuffer buffer) throws Exception {


        //可读长度必须大于基本长度
        if (buffer.readableBytes() >= BASE_LENGTH) {
            //记录包头开始的index  0 <= readIndex <= writeIndex
            int beginReader = buffer.readerIndex();

            //读到包头
            while (true) {
                if (buffer.readInt() == ServerSetting.FLAG) {
                    break;
                }
            }
            //读time
            int time = buffer.readInt();

            //读length 减去cmd
            int length = buffer.readShort() - 2;
            //读cmd
            short cmd = buffer.readShort();
            //读data
            if (buffer.readableBytes() < length) {
                //还原读指针 下次调用时再从包头开始
                buffer.readerIndex(beginReader);
                return null;
            }
            byte[] data = new byte[length];
            //buffer.readBytes(length);
            buffer.readBytes(data);
            //ServerLog.logger.debug(NumberUtils.byteToHexString(data));

            //新建请求对象
            Request request = new Request();
            request.setTime(time);
            request.setCmd(cmd);
            request.setData(data);

            //往下传递
            return request;
        } else {
            //数据包不完整 等待后面的包
            return null;
        }


    }


}
