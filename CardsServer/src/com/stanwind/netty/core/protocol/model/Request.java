package com.stanwind.netty.core.protocol.model;


/**
 * 请求对象
 *
 * @author westwind
 */
public class Request {

    /**
     * 时间
     */
    private int time;

    /**
     * 命令号
     */
    private short cmd;

    /**
     * 数据部分
     */
    private byte[] data;

    public short getCmd() {
        return cmd;
    }

    public void setCmd(short cmd) {
        this.cmd = cmd;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public short getLength() {
        short length = (short) (data.length + 2);
        return length;
    }
}
