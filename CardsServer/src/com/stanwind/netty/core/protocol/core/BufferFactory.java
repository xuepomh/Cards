package com.stanwind.netty.core.protocol.core;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

import java.nio.ByteOrder;

/**
 * buffer工厂
 *
 * @author StanWind
 */
public class BufferFactory {
    //大端序列
    public static final ByteOrder BYTE_ORDER = ByteOrder.BIG_ENDIAN;

    /**
     * 获取buffer
     *
     * @return ChannelBuffer
     */
    public static ChannelBuffer getBuffer() {
        return ChannelBuffers.dynamicBuffer();
    }

    /**
     * 将数据写入buffer
     *
     * @param bytes
     * @return
     */
    public static ChannelBuffer getBuffer(byte[] bytes) {
        return ChannelBuffers.copiedBuffer(bytes);
    }

}
