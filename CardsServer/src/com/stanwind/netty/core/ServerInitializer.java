package com.stanwind.netty.core;

import com.stanwind.netty.core.protocol.codc.RequestDecoder;
import com.stanwind.netty.core.protocol.codc.ResponseEncoder;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by StanWind on 2017/2/16.
 */
@Service("serverInitializer")
public class ServerInitializer implements ChannelPipelineFactory{
    @Autowired
    private Router router;

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = Channels.pipeline();

    //                    // IdleStateHanler
    //                    pipeline.addLast("idle", new
    //                            IdleStateHandler(hashedWheelTimer, 60, 60, 120));
        //Receive Decoder
        pipeline.addLast("decoder", new RequestDecoder());
        // Handler
        pipeline.addLast("Router", router);
        // Send Encoder
        pipeline.addLast("encoder", new ResponseEncoder());
        return pipeline;
}

}
