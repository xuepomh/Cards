package com.stanwind.netty.core.subscribe;

import redis.clients.jedis.BinaryJedisPubSub;

/**
 * redis 字节订阅监听器
 *
 * @author StanWind
 */
public class BinaryRedisPubSubListener extends BinaryJedisPubSub {

//    public PlayerCustomer sub;
//
//    public PlayerCustomer getSub() {
//        return sub;
//    }
//
//    public void setSub(PlayerCustomer sub) {
//        this.sub = sub;
//    }

    // 取得订阅的消息后的处理
    @Override
    public void onMessage(byte[] channel, byte[] msg) {
//        Log.getLogger().debug("onMessage: channel[" + new String(channel) + "],"
//                + " message[" + NumberUtils.byteToHexString(msg) + "]");
//        sub.getChannel().write(msg);
    }

    // 取得按表达式的方式订阅的消息后的处理   
    @Override
    public void onPMessage(byte[] parrten, byte[] channel, byte[] msg) {
//        String c = new String(channel);
//        //  控制台消息
//        if (c.equals("MsgTerminal")) {                      //	控制台传来的指令
//            int roomID = Integer.valueOf(new String(msg));  //	得到房间号
//            Room room = RoomManager.getRoom(roomID);        //	得到房间
//            if (room == null)                               //  房间不为空
//                return;
//            RedisUtil.publishData("Packer".getBytes(), (room.toString()).getBytes());    //msg接受
//            //Log.getLogger().info(room.toString());
//            return;
//        } else if (c.equals("Packer")) {    //  输出控制台消息
//            Log.getLogger().info(new String(msg));
//        } else if (c.equals("Announce")) {
//            Log.getLogger().info("Announce--->" + msg);
//            String temp = new String(msg);
//            String[] ann = temp.split("#");
//            if (ann[0].equals("-1")) {
//                GameManager.sendAnnounceMsg(ann[1]);
//            } else {
//                GameManager.sendAnnounceMsg(ann[1], Integer.valueOf(ann[0]));
//            }
//        } else {    //  发送给客户端的消息
//            // 是否把订阅发送的Packer数据传送到MsgTerminal单独显示
//            if (GameSetting.MSGLOG) {
//                RedisUtil.publishData("Packer".getBytes(), (
//                        sub.getNickname() + ": channel[" + new String(channel) + "],"
//                                + " message[" + NumberUtils.byteToHexString(msg) + "]"
//                ).getBytes());
//            } else {
//                Log.getLogger().info(sub.getNickname() + ": channel[" + new String(channel) + "],"
//                        + " message[" + NumberUtils.byteToHexString(msg) + "]");
//            }
//
//            //写数据
//            if (sub.getChannel().isOpen() && sub.getChannel().isWritable()) {
//                sub.getChannel().write(msg);
//            } else {
//                Log.getLogger().error("订阅---->不可到达的推送[" + sub.getNickname() + "] 由于channel断开");
//                EXITROOM.process(sub);  //  退出房间
//                sub.unsub();            //  断线
//            }
//        }


    }

    // 初始化按表达式的方式订阅时候的处理
    @Override
    public void onPSubscribe(byte[] channel, int subscribedChannels) {

    }

    // 取消按表达式的方式订阅时候的处理
    @Override
    public void onPUnsubscribe(byte[] channel, int subscribedChannels) {

    }

    // 初始化订阅时候的处理
    @Override
    public void onSubscribe(byte[] parrten, int subscribedChannels) {
//        Log.getLogger().debug("sub: parrten[" + new String(parrten) + "],"
//                + "sub channel[" + subscribedChannels + "]");

    }

    // 取消订阅时候的处理
    @Override
    public void onUnsubscribe(byte[] parrten, int subscribedChannels) {
//		ServerLog.getLogger().debug("unSub: parrten[" + new String(parrten) + "],"
//		 		+ "sub channel[" + subscribedChannels + "]");  

    }

}
