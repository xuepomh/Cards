package com.stanwind.service;

import com.stanwind.domain.model.Character;

/**
 * Created by StanWind on 2017/2/15.
 */
public interface CharacterService {
    int deleteByAccountIdKey(String accountId);

    int insert(Character record);

    Character selectByAccountId(String accountId);

    int updateByAccountId(Character record);

    Character selectByNickName(String nickname);
}
