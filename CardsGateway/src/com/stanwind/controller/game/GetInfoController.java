package com.stanwind.controller.game;

import com.google.gson.Gson;
import com.stanwind.api.Log;
import com.stanwind.api.TokenUtils;
import com.stanwind.constant.ResponseState;
import com.stanwind.domain.model.Account;
import com.stanwind.domain.model.Character;
import com.stanwind.domain.vo.PlayerInfoVo;
import com.stanwind.service.AccountService;
import com.stanwind.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by StanWind on 2017/2/15.
 */
@Controller
public class GetInfoController {

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CharacterService characterService;

    @Autowired
    private Log log;

    @RequestMapping(value = "getInfo", method = RequestMethod.POST)//,produces = "application/json;charset=UTF-8"
    public
    @ResponseBody
    Object getInfo(HttpServletRequest request) throws Exception {
        String token = request.getParameter("token");
        String accountId = tokenUtils.getAccountIdByToken(token);
        Account account = this.accountService.getAccountByAccountID(accountId);

        //先检查character里有没有信息
        Character character = this.characterService.selectByAccountId(accountId);
        //需要先完善账户信息
        if (character == null)
            return ResponseState.GET_INFO_NULL.convertToJsonObject();

        //属性拷贝到vo
        PlayerInfoVo playerInfoVo = new PlayerInfoVo();
        playerInfoVo.setAccount(account);
        playerInfoVo.setCharacter(character);

        log.getLogger().debug("nickname->" + playerInfoVo.getNickname());
        //log.getLogger().debug(account + "-" + accountId + "-" + username);
        Gson gson = new Gson();
        return gson.toJson(playerInfoVo);
    }

}
