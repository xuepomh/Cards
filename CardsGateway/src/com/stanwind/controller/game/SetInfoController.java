package com.stanwind.controller.game;

import com.stanwind.api.TokenUtils;
import com.stanwind.constant.ResponseState;
import com.stanwind.constant.ServerSetting;
import com.stanwind.domain.model.Character;
import com.stanwind.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by StanWind on 2017/2/15.
 */
@Controller
public class SetInfoController {

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private CharacterService characterService;

    @RequestMapping("setInfo")
    public @ResponseBody Object setInfo(HttpServletRequest request) {
        String token = request.getParameter("token");
        String nickname = request.getParameter("nickname");
        String head = request.getParameter("head");
        String accountId = tokenUtils.getAccountIdByToken(token);
        Character character = this.characterService.selectByAccountId(accountId);
        if (character == null) {
            //TODO 未设置名字
            if(nickname == null)
                return ResponseState.SET_INFO_NAME_NULL.convertToJsonObject();


            //TODO 名字重复
            if(this.characterService.selectByNickName(nickname)!=null)
                return ResponseState.SET_INFO_REPEAT.convertToJsonObject();

            //TODO 第一次进入游戏设置
            character = new Character();
            character.setAllTimes(0);
            character.setWinTimes(0);
            character.setDiamond(ServerSetting.INIT_DIAMOND);
            character.setGold(ServerSetting.INIT_GOLD);
            character.setIconSource("1");
            character.setNickname(nickname);
            character.setAccountId(accountId);

            this.characterService.insert(character);
            return ResponseState.INFO_OK.convertToJsonObject();
        } else {
            //TODO 更改信息
            return ResponseState.INFO_OK.convertToJsonObject();
        }

    }

}
