package com.stanwind.controller.commons;

import com.alibaba.fastjson.JSONObject;
import com.stanwind.api.CommonUtils;
import com.stanwind.api.Log;
import com.stanwind.api.NumberUtils;
import com.stanwind.api.TokenUtils;
import com.stanwind.constant.ResponseState;
import com.stanwind.domain.model.Account;
import com.stanwind.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

//import org.springframework.beans.BeanUtils;

/**
 * Created by StanWind on 2017/2/14.
 */
@Controller
@RequestMapping("ptlogin")
public class LoginController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private Log log;

    @Autowired
    private NumberUtils numberUtils;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private CommonUtils commonUtils;

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public @ResponseBody Object login(HttpServletRequest request, Model model) throws Exception {
        String username = request.getParameter("username");
        String password = request.getParameter("password");


        if (username == null || username.equals("") || password == null || password.equals(""))
            return ResponseState.ACCOUNT_NULL.convertToJsonObject();


        Account account = this.accountService.getAccountByAccount(username);
        if (account == null)
            return ResponseState.ACCOUNT_NOT_EXIT.convertToJsonObject();

        log.getLogger().debug("Raw-->pwd[" + password + "] salt[" + account.getSalt() + "]"
                + " MD5-->pwd[" + numberUtils.MD5(password) + "]"
                + " salt[" + numberUtils.MD5(password + account.getSalt()) + "]");

        boolean check = account.getPasswordMd5().equals(numberUtils.MD5(password + account.getSalt()));
        if (check) {
            //TODO 登录成功 不直接返回信息了
            //Gson gson = new Gson();
            //AccountVo accountVo = new AccountVo();
            //BeanUtils.copyProperties(accountVo,account);
            //return gson.toJson(accountVo);

            //return ResponseState.ACCOUNT_OK.convertToJsonObject();
            //TODO 更新登录的IP和时间
            account.setLastLoginIp(commonUtils.getIpAddress(request));
            account.setLastLoginTime(numberUtils.getCurrentDate());
            accountService.update(account);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code",ResponseState.ACCOUNT_OK.getCode());
            jsonObject.put("token", tokenUtils.generateToken(account.getAccountId()));
            return jsonObject;

        } else {
            return ResponseState.ACCOUNT_ERROR.convertToJsonObject();
        }


    }


}
