package com.stanwind.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Hex类工具
 *
 * @author StanWind
 */
@Component
public class NumberUtils {
    @Autowired
    private Log log;
    //Integer.toHexString(b & 0xff)

    /**
     * 字节集到十六进制文本
     *
     * @param bytes 字节集
     * @return
     */
    public String toHexString(byte[] bytes) {
        StringBuilder buf = new StringBuilder(32);
        int t;
        for (int i = 0; i < bytes.length; i++) {
            t = bytes[i];
            if (t < 0) {
                t += 256;
            }
            if (t < 16) {
                buf.append("0");
            }
            buf.append(Integer.toHexString(t));
        }
        return buf.toString().toUpperCase();
    }

    /**
     * 字节集到十六进制文本【源字节间添加空格】
     *
     * @param bytes 字节集
     * @return
     */
    public String toHexStringHandle(byte[] bytes) {
        return toHexString(bytes).replaceAll("(.{2})", "$1 ").trim();
    }

    /**
     * 字节集到十六进制文本
     *
     * @param bytes 字节集
     * @return
     */
    public String byteToHexString(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder(32);
        for (int i = 0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            String hv = Integer.toHexString(v).toUpperCase();
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv + " ");
        }
        return stringBuilder.toString();
    }

    /**
     * 十六进制文本到字节集
     *
     * @param hexString 十六进制文本
     * @return
     */
    public byte[] hexStringToBytes(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        // 去除空格、制表符、换行、回车
        hexString = hexString.replaceAll("\\s*|\t|\r|\n", "");
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] bytes = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            bytes[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return bytes;
    }

    public byte charToByte(char i) {
        return (byte) "0123456789ABCDEF".indexOf(i);
    }


    /**
     * 密码加密算法 不可重写
     *
     * @param s 明文
     * @return 密文
     */
    public final String MD5(String s) {
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            log.getLogger().info("MD5 error---->" + e.toString());
            return null;
        }
    }

    /**
     * 取指定范围随机数
     *
     * @param min 最小值
     * @param max 最大值
     * @return 随机生成的数
     */
    public int getRandomInt(int min, int max) {
        if (max < min)
            return -1;

        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return s;
    }

    /**
     * 返回当前时间
     * @return 文本型时间
     */
    public String getCurrentTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String time = df.format(new Date());// new Date()为获取当前系统时间
        return time;
    }
    /**
     * 返回当前日期
     * @return 文本型日期时间
     */
    public String getCurrentDate(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
        String time = df.format(new Date());// new Date()为获取当前系统时间
        return time;
    }
}
