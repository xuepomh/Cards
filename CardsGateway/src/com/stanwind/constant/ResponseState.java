package com.stanwind.constant;


import com.alibaba.fastjson.JSONObject;

/**
 * Created by StanWind on 2017/2/14.
 */
public enum ResponseState {

    //登录类
    ACCOUNT_OK(1000,"登录成功"),
    ACCOUNT_NULL(1001,"请输入账号密码"),
    ACCOUNT_NOT_EXIT(1002,"账号不存在"),
    ACCOUNT_ERROR(1003,"账号或密码错误"),

    //注册类
    REG_NULL(2003,"请填写完整的账号密码"),
    REG_OK(2000,"注册成功"),
    REG_EXIST(2001,"账号已存在"),
    REG_ERROR(2002,"注册失败,SQL Error"),

    //游戏内类
    INFO_OK(3000,"成功"),
    SET_INFO_NAME_NULL(3003,"请先填写玩家名字"),
    SET_INFO_NULL(3004,"没有提交数据"),
    SET_INFO_REPEAT(3005,"名字已存在"),

    GET_INFO_NULL(3001,"请先填写玩家名字"),
    GET_INFO_FAIL(3002,"获取信息失败"),


    //鉴权类
    VERIFY_FAIL(4001,"token过期,请重新登陆");



    private String tip;
    private int code;

    public int getCode() {
        return code;
    }

    ResponseState(int code, String tip){
        this.code = code;
        this.tip = tip;
    }
    public JSONObject convertToJsonObject(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",this.code);
        jsonObject.put("tip",this.tip);

        return jsonObject;
    }
}
