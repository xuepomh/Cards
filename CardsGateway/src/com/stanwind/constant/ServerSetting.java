package com.stanwind.constant;

/**
 * Created by StanWind on 2017/2/14.
 */
public interface ServerSetting {
    /**
     * LOG4J 文件位置
     */
    String LOG4JPATH = "log4j.properties";

    /**
     * accountid 前缀
     */
    String ACCID_PREFIX = "cards_";

    /**
     * 用于生成token
     */
    String TOKENKEY = "stanwind";
    /**
     * 新注册账号类型
     */
    int ACC_TYPE = 1;

    /**
     * 过期时间 单位 秒
     */
    int EXPIRE_TIME = 60000;


    //~~~~~~~~~`游戏
    /**
     *  初始化金币
     */
    int INIT_GOLD = 3000;

    /**
     * 初始化钻石
     */
    int INIT_DIAMOND = 0;

    /**
     * 服务器名前缀
     */
    String SERVER_NAME_PRE = "cards:server:";




}
