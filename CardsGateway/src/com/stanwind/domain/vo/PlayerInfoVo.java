package com.stanwind.domain.vo;

import com.stanwind.domain.model.Account;
import com.stanwind.domain.model.Character;

/**
 * Created by StanWind on 2017/2/15.
 */
public class PlayerInfoVo {
    //Account
    private String accountId;

    private String account;

    private String blockedTime;

    private String blockedReason;

    private String tel;

    private String email;

    //Characters
    private String nickname;

    private Integer gold;

    private Integer diamond;

    private String iconSource;

    private Integer winTimes;

    private Integer allTimes;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBlockedTime() {
        return blockedTime;
    }

    public void setBlockedTime(String blockedTime) {
        this.blockedTime = blockedTime;
    }

    public String getBlockedReason() {
        return blockedReason;
    }

    public void setBlockedReason(String blockedReason) {
        this.blockedReason = blockedReason;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getDiamond() {
        return diamond;
    }

    public void setDiamond(Integer diamond) {
        this.diamond = diamond;
    }

    public String getIconSource() {
        return iconSource;
    }

    public void setIconSource(String iconSource) {
        this.iconSource = iconSource;
    }

    public Integer getWinTimes() {
        return winTimes;
    }

    public void setWinTimes(Integer winTimes) {
        this.winTimes = winTimes;
    }

    public Integer getAllTimes() {
        return allTimes;
    }

    public void setAllTimes(Integer allTimes) {
        this.allTimes = allTimes;
    }


    /**
     * 拷贝account对象属性到vo
     *
     * @param account Account对象
     */
    public void setAccount(Account account) {
        setAccount(account.getAccount());
        setAccountId(account.getAccountId());
        setBlockedTime(account.getBlockedTime());
        setBlockedReason(account.getBlockedReason());
        setTel(account.getTel());
        setEmail(account.getEmail());
    }

    /**
     * 拷贝character对象属性到vo
     *
     * @param character Character对象
     */
    public void setCharacter(Character character) {
//        //目标对象为空 或者目标对象的AccountId和当前的不一致 不允许拷贝
//        if (character == null || !character.getAccountId().equals(this.getAccountId()))
//            return false;

        setNickname(character.getNickname());
        setGold(character.getGold());
        setDiamond(character.getDiamond());
        setIconSource(character.getIconSource());
        setWinTimes(character.getWinTimes());
        setAllTimes(character.getAllTimes());

    }
}
