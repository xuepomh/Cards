package com.stanwind.domain.dao;

import com.stanwind.domain.model.Character;

public interface CharacterMapper {
    int deleteByPrimaryKey(String accountId);

    int insert(Character record);

    int insertSelective(Character record);

    Character selectByPrimaryKey(String accountId);

    Character selectByNickName(String nickname);

    int updateByPrimaryKeySelective(Character record);

    int updateByPrimaryKey(Character record);
}